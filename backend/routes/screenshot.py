import base64
from typing import Dict
from fastapi import APIRouter
from pydantic import BaseModel
import httpx

router = APIRouter()


# def bytes_to_data_url(image_bytes: bytes, mime_type: str) -> str:
#     base64_image = base64.b64encode(image_bytes).decode("utf-8")
#     return f"data:{mime_type};base64,{base64_image}"


# async def capture_screenshot(
#     target_url: str, api_key: str, device: str = "desktop"
# ) -> bytes:
#     api_base_url = "https://api.screenshotone.com/take"

#     params = {
#         "access_key": api_key,
#         "url": target_url,
#         "full_page": "true",
#         "device_scale_factor": "1",
#         "format": "png",
#         "block_ads": "true",
#         "block_cookie_banners": "true",
#         "block_trackers": "true",
#         "cache": "false",
#         "viewport_width": "342",
#         "viewport_height": "684",
#     }

#     if device == "desktop":
#         params["viewport_width"] = "1280"
#         params["viewport_height"] = "832"

#     async with httpx.AsyncClient(timeout=60) as client:
#         response = await client.get(api_base_url, params=params)
#         if response.status_code == 200 and response.content:
#             return response.content
#         else:
#             raise Exception("Error taking screenshot")

def convert_image_to_data_url(image_bytes: bytes, mime_type: str) -> str:
    base64_image = base64.b64encode(image_bytes).decode("utf-8")
    return f"data:{mime_type};base64,{base64_image}"

async def take_screenshot(
    url: str, api_key: str, device: str = "desktop"
) -> bytes:
    params = build_screenshot_params(api_key, url, device)
    API_BASE_URL = "https://api.screenshotone.com/take"
    
    async with httpx.AsyncClient(timeout=60) as client:
        response = await client.get(API_BASE_URL, params=params)
        return handle_api_response(response)

def build_screenshot_params(
    api_key: str, url: str, device: str
) -> Dict[str, str]:
    params = {
        "access_key": api_key,
        "url": url,
        "full_page": "true",
        "device_scale_factor": "1",
        "format": "png",
        "block_ads": "true",
        "block_cookie_banners": "true",
        "block_trackers": "true",
        "cache": "false",
        "viewport_width": "342",
        "viewport_height": "684",
    }

    if device == "desktop":
        params["viewport_width"] = "1280"
        params["viewport_height"] = "832"
    
    return params

def handle_api_response(response: httpx.Response) -> bytes:
    if response.status_code == 200 and response.content:
        return response.content
    else:
        raise Exception("Error taking screenshot")


class ScreenshotRequest(BaseModel):
    url: str
    apiKey: str


class ScreenshotResponse(BaseModel):
    url: str


@router.post("/api/screenshot")
async def app_screenshot(request: ScreenshotRequest):
    # Extract the URL from the request body
    url = request.url
    api_key = request.apiKey

    # TODO: Add error handling
    image_bytes = await take_screenshot(url, api_key=api_key)

    # Convert the image bytes to a data url
    data_url = convert_image_to_data_url(image_bytes, "image/png")

    return ScreenshotResponse(url=data_url)
