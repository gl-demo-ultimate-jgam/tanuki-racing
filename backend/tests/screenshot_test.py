import pytest
from fastapi.testclient import TestClient
from backend.routes.screenshot import app # the FastAPI app

@pytest.mark.asyncio
async def test_screenshot_success():
    client = TestClient(app)
    response = await client.post("/api/screenshot", json={
        "url": "https://www.example.com",
        "apiKey": "abc123" 
    })
    assert response.status_code == 200
    assert "data:image/png;base64" in response.json()["url"]

@pytest.mark.asyncio    
async def test_screenshot_bad_url():
    client = TestClient(app)
    response = await client.post("/api/screenshot", json={
        "url": "badurl",
        "apiKey": "abc123"
    })
    assert response.status_code == 500
    
@pytest.mark.asyncio
async def test_screenshot_missing_api_key():
    client = TestClient(app)
    response = await client.post("/api/screenshot", json={
        "url": "https://www.example.com",
    })
    assert response.status_code == 422