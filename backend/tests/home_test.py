import pytest
from fastapi.testclient import TestClient
from backend.routes.home import router


@pytest.fixture
def client():
    return TestClient(router)


def test_get_status(client):
    response = client.get("/")
    assert response.status_code == 200
    assert "<h3>" in response.text


def test_get_status_content(client):
    response = client.get("/")
    expected_content = "<h3>Your backend is running correctly. Please open the front-end URL (default is http://localhost:5173) to use screenshot-to-code.</h3>"
    assert expected_content in response.text

def test_get_status_returns_404(client):
    response = client.get("/invalid-path")
    assert response.status_code == 404