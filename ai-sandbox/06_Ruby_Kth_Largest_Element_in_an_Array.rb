# Link to test answer on LeetCode: https://leetcode.com/problems/kth-largest-element-in-an-array/?envType=featured-list

# Kth Largest Element in an Array Problem: We are given an integer array nums and an integer k, return the kth largest element in the array. Provide two different solutions for us to choose from.